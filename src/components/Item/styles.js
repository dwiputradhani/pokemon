import styled from '@emotion/styled';

export const Item = styled.div`
  padding: 10px;
  border-radius: 5px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  height: 100px;
  border: solid 1px #e0e0e0;
  background: ${({ bg }) => bg}
`;

export const Image = styled.img`
  width: 50px;
`;

export const ButtonRelease = styled.button`
  padding: 5px 10px;
  border: none;
  background: #f14141;
  border-radius: 5px;
  color: #fff;
  position: absolute;
  top: -1px;
  right: 0;
  cursor: pointer;
  &:hover{
    background: #d92727;
  }
`;

export const TextLabel = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  h5 {
    color: #232323;
    margin: 0 0 5px 0;
    font-size: 15px;
    text-transform: capitalize;
  }
  p {
    color: #232323;
    margin: 0;
    font-size: 13px;
    background: #ffffff94;
    padding: 2px 10px;
    border-radius: 10px;
    text-align: center;
    margin-bottom: 4px;
  }
  
`;