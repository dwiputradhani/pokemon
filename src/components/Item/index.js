import React from 'react'
import { ItemContainer } from '../styles/Container.styles';
import { Item, Image, TextLabel, ButtonRelease } from './styles';
import { Link } from 'react-router-dom';
import { bg } from '../../utils/utils_bg';

export default function index({ name, type, image, types, called, setPokemon }) {
  const handleRelease = (namex) => {
    let pokelist = JSON.parse(localStorage.getItem('mypokemon')) || [];
    let idx = pokelist.map(function (e) { return e.called_name; }).indexOf(namex);
    let idxs = pokelist.map(function (e) { return e.name; }).indexOf(name);
    console.log(pokelist, idxs);
    pokelist.splice(idx, 1);
    setPokemon(pokelist);
    localStorage.setItem('mypokemon', JSON.stringify(pokelist));
  }
  const renderOwnTotal = () => {
    let pokelist = JSON.parse(localStorage.getItem('mypokemon')) || [];
    let total = 0;
    pokelist.forEach(item => {
      if (item.name === name) {
        total = total + 1;
      }
    })
    return total;
  }
  return (
    <ItemContainer>
      {called && <ButtonRelease onClick={() => handleRelease(called)}>Remove</ButtonRelease>}
      {!called && <ButtonRelease> Owned: {renderOwnTotal()}</ButtonRelease>}
      <Link to={'/pokemon/' + name + '/' + type}>
        <Item bg={bg[type]}>
          <TextLabel>
            <h5>{name}</h5>
            {called ? <h5>"{called}"</h5> : ""}
            {
              types.map(item =>
                <p key={item.slot}>{item.type.name}</p>
              )
            }
          </TextLabel>
          <Image src={image} />
        </Item>
      </Link>
    </ItemContainer>
  )
}
