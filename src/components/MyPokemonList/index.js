import React, { useState } from 'react'
import { Container, TitleContainer, Wrapper } from '../styles/Container.styles';
import Item from '../Item';

const Index = () => {

  const [allPokemons, setAllPokemons] = useState(JSON.parse(localStorage.getItem('mypokemon')) || []);

  return (
    <Wrapper>
      <TitleContainer>
        <h1>My Pokemon List</h1>
      </TitleContainer>
      <Container>
        {
          allPokemons.map((item, index) => {
            return (
              <Item
                called={item.called_name || ''}
                image={item.sprites.other.dream_world.front_default}
                key={index}
                name={item.name}
                setPokemon={setAllPokemons}
                type={item.types[0].type.name}
                types={item.types}
              />
            )
          })
        }
      </Container>
    </Wrapper>
  );
}

export default Index;