import styled from '@emotion/styled';

export const BottomTab = styled.div`
  height: 50px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  position: fixed;
  background: #fff;
  width: 100%;
  bottom: 0;
  box-shadow: rgb(108 114 124 / 16%) 0px -2px 4px 0px;
  a{
    text-decoration: none;
  }
`;
export const BottomTabItem = styled.div`
  flex: 1;
  align-items: center;
  text-align: center;
`;
export const TitleItem = styled.p`
  font-size: 13px;
  font-weight: 500;
  color: ${({ color }) => color}
`;