import React from 'react';
import { BottomTab, BottomTabItem, TitleItem } from './styles';
import { useLocation, Link } from 'react-router-dom';
let route = "";
function HeaderView() {
  const location = useLocation();
  route = location.pathname;
}
function index() {
  let restrictRoute = ['/', '/my-pokemon'];
  HeaderView();
  return (
    restrictRoute.includes(route) &&
    <BottomTab>
      <BottomTabItem>
        <Link to={'/'}>
          <TitleItem color={route === '/' ? '#f14141' : '#232323'}>PokeList</TitleItem>
        </Link>
      </BottomTabItem>
      <BottomTabItem>
        <Link to={'/my-pokemon'}>
          <TitleItem color={route === '/my-pokemon' ? '#f14141' : '#232323'}>My Pokemon</TitleItem>
        </Link>
      </BottomTabItem>
    </BottomTab>
  );
}

export default index;