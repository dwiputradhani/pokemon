import React, { useEffect, useState } from 'react'
import { Container, TitleContainer, Wrapper } from '../styles/Container.styles';
import { ButtonLoad } from './styles';
import Item from '../Item';

const Index = () => {

  const [allPokemons, setAllPokemons] = useState([])
  const [loadMore, setLoadMore] = useState('https://pokeapi.co/api/v2/pokemon?limit=10');

  const getAllPokemons = async () => {
    const res = await fetch(loadMore)
    const data = await res.json()
    setLoadMore(data.next)
    function createPokemonObject(results) {
      results.forEach(async pokemon => {
        const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${pokemon.name}`)
        const data = await res.json()
        setAllPokemons(currentList => [...currentList, data])
      })
    }
    createPokemonObject(data.results);
  }

  useEffect(() => {
    getAllPokemons();
  }, [])

  return (
    <Wrapper>
      <TitleContainer>
        <h1>Pokemon List</h1>
      </TitleContainer>
      <Container>
        {
          allPokemons.map((item, index) => {
            return (
              <Item
                image={item.sprites.other.dream_world.front_default}
                key={index}
                name={item.name}
                type={item.types[0].type.name}
                types={item.types}
              />
            )
          })
        }
        <ButtonLoad onClick={() => getAllPokemons()}>Load more</ButtonLoad>
      </Container>
    </Wrapper>
  );
}

export default Index;