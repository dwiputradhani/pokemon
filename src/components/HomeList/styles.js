import styled from '@emotion/styled';

export const ButtonLoad = styled.button`
  background: #f8f8f8;
  padding: 10px 20px;
  border: solid 1px #e0e0e0;
  border-radius: 5px;
  margin-top: 20px;
  margin-bottom: 60px;
  box-shadow: rgb(108 114 124 / 16%) 0px -2px 4px 0px;
  width: 100%;
`;