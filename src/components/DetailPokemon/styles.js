import styled from '@emotion/styled';

export const DetailContainer = styled.div`
  padding: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-end;
  height: 300px;
  background: ${({ bg }) => bg}
`;
export const ButtonBack = styled.div`
  background: #ffffff94;
  border-radius: 50px;
  width: 50px;
  height: 50px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-weight: bold;
  top: 10px;
  left: 10px;
  position: absolute;
  cursor: pointer;
`;

export const StatContainer = styled.div`
  padding: 10px;
  display: flex;
  margin-top: -40px;
  border-radius: 50px 50px 0 0;
  flex-direction: column;
  justify-content: space-around;
  background: #fff;
  padding-top: 50px;

  h5{
    text-transform: capitalize;
    font-weight: bold;
    font-size: 20px;
    margin: 0 0;
    text-align: center;
  }

  h6{
    text-align: left;
    font-size: 18px;
    margin: 0 0;
  }
`;

export const TypesContainer = styled.div`
  padding: 10px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  p{
    text-transform: capitalize;
    font-weight: bold;
    font-size: 20px;
    margin: 0 0;
    font-size: 13px;
    background: #7cbeb340;
    padding: 2px 10px;
    border-radius: 10px;
    text-align: center;
    margin-bottom: 5px;
  }
`;
export const ListMoveContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  margin-top: 10px;
  padding-bottom: 50px;
  p{
    text-transform: capitalize;
    font-weight: bold;
    font-size: 20px;
    margin: 0 0;
    font-size: 13px;
    padding: 5px 10px;
    flex: 20%;
    border-radius: 5px;
    text-align: center;
    margin-bottom: 5px;
    background: ${({ bg }) => bg};
  }
`;