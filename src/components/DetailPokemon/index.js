import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { DetailContainer, StatContainer, TypesContainer, ListMoveContainer, ButtonBack } from './styles';
import { Wrapper } from '../../components/styles/Container.styles';
import { ButtonPrimary, ButtonPrimaryNormal, ButtonSecondaryNormal } from '../../components/styles/Button.styles';
import { ModalContainer, ModalBody } from '../../components/styles/Modal.styles';
import { bg } from '../../utils/utils_bg';
import Arrow from '../../assets/arrow.svg';
import { useNavigate } from 'react-router-dom';

export default function Index() {
  let { id, type } = useParams();
  const [pokemonInfo, setPokemonInfo] = useState({});
  const [catchingProcess, setCatchingProcess] = useState(false);
  const [catchingModal, setCatchingModal] = useState(false);
  const [pokemonName, setPokemonName] = useState('');
  const navigate = useNavigate();

  const getPokemonInfo = async () => {
    const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`)
    const data = await res.json()
    setPokemonInfo(data);
  }

  useEffect(() => {
    getPokemonInfo();
  }, [])

  const handleCatch = () => {
    setCatchingModal(true);
    setCatchingProcess(true);
    setTimeout(() => {
      setCatchingProcess(false);
    }, 1000);
  };

  const saveCatch = () => {
    let currentStorage = JSON.parse(localStorage.getItem('mypokemon')) || [];
    let pokemonCatch = { ...pokemonInfo, called_name: pokemonName };
    currentStorage.push(pokemonCatch);
    localStorage.setItem('mypokemon', JSON.stringify(currentStorage));
    setCatchingModal(false);
    setCatchingProcess(false);
    setPokemonName('');
  };

  return (
    <Wrapper>
      <ButtonBack onClick={() => navigate(-1)}>
        <img src={Arrow} width={25} height={25} alt='back' />
      </ButtonBack>
      <DetailContainer bg={bg[type]}>
        <img src={pokemonInfo?.sprites?.other?.dream_world.front_default} width={200} height={200} alt='pokemon' />
      </DetailContainer>
      <StatContainer>
        <h5>{pokemonInfo.name}</h5>
        <TypesContainer>
          {
            pokemonInfo?.types?.map(item =>
              <p key={item.slot}>{item.type.name}</p>
            )
          }
        </TypesContainer>
        <h6>Moves:</h6>
        <ListMoveContainer bg={bg[type]}>
          {
            pokemonInfo?.moves?.map(item =>
              <p key={item.move.name}>{item.move.name}</p>
            )
          }
        </ListMoveContainer>
        <ButtonPrimary>
          <button onClick={handleCatch}>
            Catch Pokemon
          </button>
        </ButtonPrimary>
      </StatContainer>
      {
        catchingModal &&
        <ModalContainer>

          <ModalBody>
            {
              catchingProcess ?
                <p>Catching The Pokemon</p> :
                (
                  <>
                    <p>The Pokemon has been Catched</p>
                    <input value={pokemonName} onChange={(e) => setPokemonName(e.target.value)} placeholder='Give you Pokemon Name...' />
                    <ButtonPrimaryNormal onClick={saveCatch}>
                      <button>Save Pokemon</button>
                    </ButtonPrimaryNormal>
                    <ButtonSecondaryNormal onClick={() => setCatchingModal(false)}>
                      <button>Release</button>
                    </ButtonSecondaryNormal>
                  </>
                )
            }
          </ModalBody>

        </ModalContainer>
      }
    </Wrapper>
  )
}
