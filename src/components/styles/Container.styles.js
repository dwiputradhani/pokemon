import styled from '@emotion/styled';

export const Wrapper = styled.div`
  max-width: 500px;
  margin: 0 auto;
`;

export const Container = styled.div`
  padding: 10px 10px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;
export const TitleContainer = styled.div`
  h1{
    margin: 0;
    padding-left: 20px;
    font-size: 25px;
    margin-top: 20px;
  }
`;

export const ItemContainer = styled.div`
  flex: 40%;
  padding: 10px;
  position: relative;
  a{
    text-decoration: none;
  }
`;