import styled from '@emotion/styled';

export const ButtonPrimary = styled.div`
  height: 50px;
  padding-top: 10px;
  padding-bottom: 10px;
  display: flex;
  flex-direction: row;
  position: fixed;
  width: 100%;
  bottom: 0;
  left: 0;
  align-items: center;
  justify-content: center;

  button{
    padding: 10px 20px;
    box-shadow: rgb(108 114 124 / 16%) 0px -2px 4px 0px;
    border-radius: 50px;
    border: none;
    font-size: 15px;
    font-weight: bold;
    cursor: pointer;
    background: #f14141;
    color: #fff;
    &:hover{
      background: #bf2121;
    }
  }
`;
export const ButtonPrimaryNormal = styled.div`
  height: 50px;
  display: flex;
  flex-direction: row;
  width: 100%;
  align-items: center;
  justify-content: center;

  button{
    padding: 10px 20px;
    box-shadow: rgb(108 114 124 / 16%) 0px -2px 4px 0px;
    border-radius: 5px;
    border: none;
    font-size: 15px;
    font-weight: bold;
    cursor: pointer;
    background: #f14141;
    color: #fff;
    &:hover{
      background: #bf2121;
    }
  }
`;
export const ButtonSecondaryNormal = styled.div`
  height: 50px;
  display: flex;
  flex-direction: row;
  width: 100%;
  align-items: center;
  justify-content: center;

  button{
    padding: 10px 20px;
    box-shadow: rgb(108 114 124 / 16%) 0px -2px 4px 0px;
    border-radius: 5px;
    border: solid 1px #e0e0e0;
    font-size: 15px;
    font-weight: bold;
    cursor: pointer;
    background: #f8f8f8;
    color: #232323;
    &:hover{
      background: #f4f4f4;
    }
  }
`;