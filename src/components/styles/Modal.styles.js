import styled from '@emotion/styled';

export const ModalContainer = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: #23232340;
  z-index: 999;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ModalBody = styled.div`
  background: #fff;
  border-radius: 5px;
  padding: 20px 20px;
  width: 70%;
  text-align: center;

  input{
    padding: 10px 10px;
    margin-bottom: 10px;
  }
`;