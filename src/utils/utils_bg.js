export const bg = {
  rock: `rgb(148, 81, 81)`,
  ghost: `rgb(247, 247, 247)`,
  electric: `rgb(255 255 112)`,
  bug: `#f9c67a`,
  poison: `#e0a7f6`,
  normal: `#F4F4F4`,
  fairy: `rgba(255, 192, 203, 0863)`,
  fire: `#ffac9e`,
  grass: `#9cff98`,
  water: `#afdeff`,
}