import HomeList from './components/HomeList';
import BottomTab from './components/BottomTab';
import MyPokemonList from './components/MyPokemonList';
import DetailPokemon from './components/DetailPokemon';

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomeList />} />
        <Route path="/my-pokemon" element={<MyPokemonList />} />
        <Route path="/pokemon/:id/:type" element={<DetailPokemon />} />
      </Routes>
      <BottomTab />
    </BrowserRouter>
  );
}

export default App;
